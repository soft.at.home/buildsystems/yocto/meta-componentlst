# meta-componentlst

Tools layer containing site specific information.

## Adding the meta-componentlst layer to your build

```bash
bitbake-layers add-layer meta-componentlst
```

## Layer content

### Site.conf

### Componentlst.inc

The componentlst. inc is intended to map yocto recipe name (PN) with the correct gitlab path. This allows components in gitlab to be moved around without breaking older sop layers. To avoid one long list of components, this file includes a file for each sop layer. 

The global file component.lst can be included by a distro or in a local.conf file. Together with the correct sop layer, this will allow SAH components to build.

### Componentlst Include files

Each sop layer should have an include file in the meta-componentlst, that is included by componentlst.inc.

Available include files:
- componenlst-net-base.inc
